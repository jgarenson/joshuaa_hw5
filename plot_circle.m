function plot_circle(points)
%will plot the input points, as well as the least squares fit of the points
%Only works for a set of points on a circle

%Calculate the coefficients and residual.
[c,res] = LS_fit(points);

%report residual to user
fprintf('r = %d \n',res);

%calculate the radius of the fitted circle
radius = sqrt(c(3) + c(1)^2 + c(2)^2);

%generate a set of theta to plot circle
theta = linspace(0,2*pi);

%convert to cartisian coordinates, translate to center coordinates
x = radius*cos(theta)+c(1);
y = radius*sin(theta)+c(2);

%plot points (red circles) and fit (black line).
figure
hold on
plot(points(:,1),points(:,2),'ro')
plot(x,y)
xlabel('x')
ylabel('y')
title('Best Fit Circle')
end


function [c,r] = LS_fit(points)
%takes in a set of points (nx2 matrix) and finds a set of coefficient(c)
%for the best circular fit.

%if points does not have 2 columns, produce error
if size(points,2) ~= 2
    error('Points must be 2xn')
end

%new variable names for easier readability
x = points(:,1); y = points(:,2);

%create A
A = [2*x 2*y (zeros(length(points),1)+1)];

%create b
b = [x.^2 + y.^2];

%Find QR factorization using CGS from HW4
[Q,R] = qr(A,0);

%Solve for c using c=R^-1*Q'*b
c = inv(R)*Q'*b;

%calculate residual vector
r = b - A*c;
%want the residual as the norm
r = norm(r);
end


