%The program creates a graph of the error for different number of points in
%for approximating legendre polynomials. 

%initial number of points
N = 128;

%initialize variables
Ns = [];
max_error = [];

%run until reaching a maximum value
while N < 2^15
%add number to tally        
Ns(end+1) = N;

%calculates the x coords
x = (-N:N)'/N;
%creates matrix A for calculating polynomials
A = [x.^0 x.^1 x.^2 x.^3];
%Get the QR factorization
[Q,R] = qr(A,0);
%normalize
scale = Q(end,:);
Q = Q*diag(1 ./scale);

%exact values for first 4 legendre polynomail.
P0 = @(x)(1);
P1 = @(x)(x);
P2 = @(x)((3/2)*x.^2 -(1/2));
P3 = @(x)((5/2)*x.^3 -(3/2).*x);

%calculates error between exact and approx for each polynomail, and
%combines into one large matrix.
deltax0 = abs(P0(x)-Q(:,1));
deltax1 = abs(P1(x)-Q(:,2));
deltax2 = abs(P2(x)-Q(:,3));
deltax3 = abs(P3(x)-Q(:,4));

delta = [deltax0,deltax1,deltax2,deltax3];

%plots error figure
figure
plot(x,delta)

%adds largest error value calculated to error matrix
max_error(end+1) = max(max(delta));

%doubles number of points
N = N*2;
end

%plots error versus the number of points used in approximation
figure
plot(Ns,max_error)

